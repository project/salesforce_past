<?php

namespace Drupal\salesforce_past\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class SettingsForm extends ConfigFormBase {

  /**
   * @inheritDoc
   */
  protected function getEditableConfigNames() {
    return [
      'salesforce_past.settings',
    ];
  }

  /**
   * @inheritDoc
   */
  public function getFormId() {
    return 'settings_form';
  }

  /**
   * @inheritDoc
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('salesforce_past.settings');
    $form['events'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Events to log'),
      '#description' => $this->t('Select the events to subscribe on'),
      '#options' => [
        'PUSH_SUCCESS' => 'PUSH Success',
        'PUSH_FAIL' => 'PUSH Fail',
        'PUSH_PARAMS' => 'PUSH Params',
      ],
      '#default_value' => is_array($config->get('events')) ? $config->get('events') : [],
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * @inheritDoc
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('salesforce_past.settings')
      ->set('events', $form_state->getValue('events'))
      ->save();
  }

}