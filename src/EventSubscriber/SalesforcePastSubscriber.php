<?php

namespace Drupal\salesforce_past\EventSubscriber;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\salesforce\Event\SalesforceEvents;
use Drupal\salesforce_mapping\Event\SalesforcePushOpEvent;
use Drupal\salesforce_mapping\Event\SalesforcePushParamsEvent;
use Drupal\salesforce_mapping\PushParams;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class SalesforcePastSubscriber.
 */
class SalesforcePastSubscriber implements EventSubscriberInterface {

  /**
   * The module handler.
   */
  protected $moduleHandler;

  /**
   * Config.
   */
  protected $config;

  /**
   * The event dispatcher.
   */
  protected $eventDispatcher;

  /**
   * {@inheritDoc}
   */
  public function __construct(ModuleHandlerInterface $module_handler, ConfigFactoryInterface $config_factory, EventDispatcherInterface $event_dispatcher) {
    $this->moduleHandler = $module_handler;
    $this->config = $config_factory->get('salesforce_past.settings');
    $this->eventDispatcher = $event_dispatcher;
  }

  /**
   * {@inheritDoc}
   */
  public function salesforcePushSuccess(SalesforcePushParamsEvent $event) {

    if (!$this->moduleHandler->moduleExists('past')) {
      return [];
    }

    if (!$this->config->get('events')['PUSH_SUCCESS']) {
      return [];
    }

    $params = $event->getParams();

    $past = past_event_create('salesforce', 'api_request', 'Success');
    $past->addArgument('data', json_encode($params->getParams(), JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES));
    $past->addArgument('mapping', $params->getMapping());
    $past->save();
  }

  /**
   * {@inheritDoc}
   */
  public function salesforcePushFail(SalesforcePushOpEvent $event) {

    if (!$this->moduleHandler->moduleExists('past')) {
      return [];
    }

    if (!$this->config->get('events')['PUSH_FAIL']) {
      return [];
    }

    $past = past_event_create('salesforce', 'api_request', 'Failed');
    $params = new PushParams($event->getMapping(), $event->getEntity());
    // maybe some module is updating params
    $this->eventDispatcher->dispatch(
      SalesforceEvents::PUSH_PARAMS,
      new SalesforcePushParamsEvent($event->getMappedObject(), $params)
    );
    $past->addArgument('data', json_encode($params->getParams(), JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES));
    $past->addArgument('mapping', $event->getMapping());
    $past->save();
  }

  /**
   * {@inheritDoc}
   */
  public function salesforcePushParams(SalesforcePushParamsEvent $event) {

    if (!$this->moduleHandler->moduleExists('past')) {
      return [];
    }

    if (!$this->config->get('events')['PUSH_PARAMS']) {
      return [];
    }

    $params = $event->getParams();

    $past = past_event_create('salesforce', 'api_request', 'Push params');
    $past->addArgument('data', json_encode($params->getParams(), JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES));
    $past->addArgument('mapping', $params->getMapping());
    $past->save();
  }

  /**
   * @inheritDoc
   */
  public static function getSubscribedEvents() {
    // this always should be a final event subscriber, so added priority -1024
    return [
      SalesforceEvents::PUSH_SUCCESS => ['salesforcePushSuccess', -1024],
      SalesforceEvents::PUSH_FAIL => ['salesforcePushFail', -1024],
      SalesforceEvents::PUSH_PARAMS => ['salesforcePushParams', -1024],
    ];
  }

}